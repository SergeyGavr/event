<?php


namespace Sergeygavr\Event;


use Sergeygavr\PEvent\EventInterface;

class Event implements EventInterface
{

    public function setName(string $name): void
    {
        // TODO: Implement setName() method.
    }

    public function getName(): string
    {
        // TODO: Implement getName() method.
    }

    public function setTarget($target): void
    {
        // TODO: Implement setTarget() method.
    }

    public function getTarget()
    {
        // TODO: Implement getTarget() method.
    }

    public function setParams(array $params): void
    {
        // TODO: Implement setParams() method.
    }

    public function getParams(): array
    {
        // TODO: Implement getParams() method.
    }

    public function getParam(string $name)
    {
        // TODO: Implement getParam() method.
    }

    public function stopPropagation(bool $flag): void
    {
        // TODO: Implement stopPropagation() method.
    }

    public function isPropagationStopped(): bool
    {
        // TODO: Implement isPropagationStopped() method.
    }
}