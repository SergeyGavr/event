<?php


namespace Sergeygavr\Event;


use Sergeygavr\PEvent\EventInterface;
use Sergeygavr\PEvent\EventManagerInterface;

class EventManager implements EventManagerInterface
{

    public function attach(string $event, callable $callback, int $priority = 0): bool
    {
        // TODO: Implement attach() method.
    }

    public function detach(string $event, callable $callback): bool
    {
        // TODO: Implement detach() method.
    }

    public function clearListeners(string $event): void
    {
        // TODO: Implement clearListeners() method.
    }

    public function trigger($event, $target = null, array $params = [])
    {
        // TODO: Implement trigger() method.
    }
}